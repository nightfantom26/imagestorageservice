FROM python:3.11.7-slim

COPY ./requirements.txt /app/requirements.txt
RUN pip install -r /app/requirements.txt
COPY /src /app
COPY /configs /app_configs
WORKDIR /app
CMD  python -m uvicorn app:app  --log-config /app_configs/logging.yml --host 0.0.0.0 --port $PORT
