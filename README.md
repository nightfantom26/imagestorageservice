# Introduction

Service Processor is designed to store raw image sources and extract frames from pictures.

The assignment conditions did not describe all aspects of the application, so I made several assumptions:
1. This is a project for internal use, so in the first version you can refuse authentication.
In subsequent versions, we can add a user entity with rights schemes and group access to pictures
2. Since we need to resize the image in the first version, we limit the images so that they can fit into memory.

# What was done 
- [x] Base features
- [x] Unit tests
- [x] CI
- [x] Flexible configuration
- [x] Logging
- [x] Online monitoring
- [x] Cloud deployment


# Architecture
The project consists of two parts:
1. Stateless service with business logic
2. S3 storage that well suited for storing large files and can scale horizontally quickly


This combination of components allows the architecture to be:
- [x] Horizontally scalable
- [x] Resistant to failure


## Code architecture
Project consist of:
1. API layer
2. Business logic layer
3. Representation layer


- All logging settings are placed in the config file. This will allow us to flexibly and quickly
configure the log format and the place where we send them.
- We can flexibly configure paths to config files (see Configuration section)
- We can use various formats for storing settings and ways to override parameters
(see detailed description in the official pydantic documentation)

# Demo
For demonstration, the project was uploaded to the cloud and is available here https://bba5gvhh2k08f405gcif.containers.yandexcloud.net/docs.

## IMPORTANT
- Cloud provider has limits on maximum size of an HTTP request to a container, 
including HTTP headers and request body = 3.5MB for serverless containers. User can not change this limit.
See details in official docs https://cloud.yandex.ru/docs/serverless-containers/concepts/limits
- The project was deployed in serverless containers. The first request may take longer to complete than the others.


# What can be done better
1. Add authentication and authorization
2. Add relational DB for storing user information
3. Add more tests for full code coverage
4. Support working with large files
5. Support streaming load

# Configuration
1. logging.yml - Config file describes settings for standard python logger module
2. app_conf.yml - Config file describes settings for the service. You can override any option in this file using 
environment variables. For inner structure use __ as separator, e.g. S3__URL=0.0.0.0 

Default config location is ./config, but you can specify your own using environment variables:
1. LOG_PATH for logging.yml
2. APP_CONF_PATH for app_conf.yml


# Local run
In order to launch project locally execute next steps:
1. Create folder ~/minio/data in your home folder
2. Run
```shell
docker-compose up
```
It will launch Minio S3 storage and the service.

3. Open Minio GUI http://127.0.0.1:9090 
4. Sign in with login: test_user password: test_password
5. Create access key with params:
```plain
accessKey: fake_accessKey
secretKey: fake_secretKey
```
5. Open project API page http://127.0.0.1:8000 
6. Done

# Run tests
```shell
pip install -r requirements.txt
export PYTHONPATH=$PWD/src
python -m unittest discover -s ./tests
```