from pydantic import BaseModel


class S3Settings(BaseModel):
    url: str
    accessKey: str
    secretKey: str
    bucket: str
