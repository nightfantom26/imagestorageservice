from typing import Optional

from pydantic import BaseModel


class Status(BaseModel):
    code: int
    description: Optional[str] = None
