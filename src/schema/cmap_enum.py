from enum import StrEnum, auto


class CMapEnum(StrEnum):
    MAGMA = auto()
    INFERNO = auto()
    PLASMA = auto()
    VIRIDIS = auto()
    CIVIDIS = auto()
    TWILIGHT = auto()
    TWILIGHT_SHIFTED = auto()
    TURBO = auto()
