from typing import Optional

from pydantic import BaseModel


class ImagesList(BaseModel):
    names: list[str]
