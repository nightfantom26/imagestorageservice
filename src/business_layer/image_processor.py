import logging
from io import BytesIO
from typing import BinaryIO, Tuple

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from PIL import Image
from minio import Minio

from schema import codes
from schema.cmap_enum import CMapEnum
from schema.status import Status
from settings.app_settings import AppSettings


class ImageProcessor:

    def __init__(self, s3_client: Minio, settings: AppSettings) -> None:
        self.__s3_client = s3_client
        self.__settings = settings
        self.__logger = logging.getLogger(__name__)

    async def delete_image(self, image_id: str) -> None:
        self.check_and_create_bucket()
        self.__s3_client.remove_object(self.__settings.s3.bucket, image_id)

    async def get_list_of_image(self) -> list[str]:
        self.check_and_create_bucket()
        names = [obj.object_name for obj in self.__s3_client.list_objects(self.__settings.s3.bucket)]
        return names

    async def upload_image(self, file_name: str, byte_stream: BinaryIO) -> Status:
        self.check_and_create_bucket()
        status = None
        try:
            self.__s3_client.stat_object(self.__settings.s3.bucket, file_name)
            status = Status(code=codes.LOGICAL_ERROR, description="File exists")
        except Exception as e:
            pass

        if status is None:
            try:
                df = pd.read_csv(byte_stream)
                is_na = pd.isna(df).any().any()
                if not is_na:
                    image_np = np.array(df.iloc[:, 1:])
                    image_pil = Image.fromarray(image_np.astype(np.uint8))
                    resized_image = image_pil.resize((150, image_np.shape[0]))
                    resized_image = np.array(resized_image)

                    depth = np.array(df.iloc[:, 0])
                    depth = depth.reshape(depth.shape[0], 1)

                    concatenated_arrays = np.concatenate((depth, resized_image), axis=1)

                    resized_df = pd.DataFrame(concatenated_arrays, columns=df.columns[:151])
                    image_bytes = BytesIO()
                    resized_df.to_csv(image_bytes, index=False)
                    self.__s3_client.put_object(self.__settings.s3.bucket, file_name, BytesIO(image_bytes.getvalue()),
                                                image_bytes.getbuffer().nbytes)
                    status = Status(code=codes.OK, description="File added")
                else:
                    status = Status(code=codes.LOGICAL_ERROR, description="File is corrupted. There are empty rows")
            except Exception as e:
                self.__logger.error(e)
                status = Status(code=codes.LOGICAL_ERROR, description="Something goes wrong")
        return status

    def check_and_create_bucket(self) -> None:
        found = self.__s3_client.bucket_exists(self.__settings.s3.bucket)
        if not found:
            self.__s3_client.make_bucket(self.__settings.s3.bucket)

    async def get_image_frame(self, image_id: str, depth_min: int, depth_max: int, cmap: CMapEnum, origin: bool) -> \
    Tuple[Status, BytesIO, str]:
        self.check_and_create_bucket()
        image_bytes = None
        image_name = None
        response = None
        try:
            response = self.__s3_client.get_object(self.__settings.s3.bucket, image_id)
            df = pd.read_csv(BytesIO(response.data))
            df = df[(depth_min <= df["depth"]) & (df["depth"] <= depth_max)]
            image_bytes = BytesIO()
            if origin:
                df.to_csv(image_bytes, index=False)
                image_name = "frame.csv"
            else:
                image_np = df.to_numpy()
                image_np = image_np[:, 1:]
                my_plt = plt.figure()
                plt.axis('off')
                plt.imshow(image_np, cmap=str(cmap))
                my_plt.savefig(image_bytes, format="PNG")
                image_name = "frame.png"
            status = Status(code=codes.OK)
        except Exception as e:
            self.__logger.error(e)
            status = Status(code=codes.LOGICAL_ERROR, description="Something goes wrong")
        finally:
            if response:
                response.close()
                response.release_conn()
        return status, image_bytes, image_name
