import logging
from contextlib import asynccontextmanager

from fastapi import FastAPI, UploadFile, Response, Query
from minio import Minio
from prometheus_fastapi_instrumentator import Instrumentator
from starlette import status

from business_layer.image_processor import ImageProcessor
from schema import codes
from schema.cmap_enum import CMapEnum
from schema.images_list_schema import ImagesList
from schema.status import Status
from settings.app_settings import AppSettings
from settings.conf_reader import read_app_config


@asynccontextmanager
async def lifespan(app: FastAPI):
    logging.getLogger(__name__).info("Initializing app")
    config_dict = read_app_config()
    settings = AppSettings(**config_dict)
    app.settings = settings
    app.instrumentator.expose(app)
    logging.getLogger(__name__).info(f"Initializing S3 connection with {settings.s3.url}")
    s3 = Minio(
        settings.s3.url,
        secure=False,
        access_key=settings.s3.accessKey,
        secret_key=settings.s3.secretKey,
    )
    logging.getLogger(__name__).info("Initializing S3 connection - Done")
    app.processor = ImageProcessor(s3, app.settings)
    logging.getLogger(__name__).info("Initializing app - Done")
    yield
    logging.shutdown()


app = FastAPI(lifespan=lifespan)
app.instrumentator = Instrumentator().instrument(app)

@app.post("/image",
          status_code=201,
          responses={
              201: {"description": "File uploaded"},
              400: {"description": "Bad request", "model": Status}}
          )
async def upload_image(file: UploadFile, response: Response):
    """
    Upload image. File must be cvs file format without empty cells
    """
    settings: AppSettings = app.settings
    if file.size > settings.file_size:
        response.status_code = status.HTTP_400_BAD_REQUEST
        operational_status = Status(code=codes.LOGICAL_ERROR,
                                    description=f"File must be less then {settings.file_size} bytes")
    elif file.content_type != "text/csv":
        operational_status = Status(code=codes.LOGICAL_ERROR, description="Wrong file type")
    else:
        processor: ImageProcessor = app.processor
        operational_status = await processor.upload_image(file.filename, file.file)
    response.status_code = status.HTTP_201_CREATED if operational_status.code == codes.OK else status.HTTP_400_BAD_REQUEST

    return operational_status


@app.get("/image/{image_id}/",
         responses={
             200: {"description": "Successful response"},
             400: {"description": "Bad request", "model": Status}}
         )
async def get_image_frame(image_id: str, depth_min: int, depth_max: int, response: Response,
                          cmap: CMapEnum = CMapEnum.PLASMA,
                          origin: bool = Query(False,
                                               description="If value is true call returns raw "
                                                           "image representation in csv format, otherwise jpg")):
    """
    Get image frame. Method can return both origin csv representation and png colored image
    """
    processor: ImageProcessor = app.processor
    operational_status, image_bytes, image_name = await processor.get_image_frame(image_id, depth_min, depth_max, cmap,
                                                                                  origin)
    if operational_status.code == codes.OK:
        return Response(content=image_bytes.getvalue(), media_type="application/octet-stream",
                        headers={"content-disposition": f"attachment; filename=\"{image_name}\""})
    else:
        response.status_code = status.HTTP_400_BAD_REQUEST
        return operational_status


@app.delete("/image/{image_id}")
async def delete_image(image_id: str):
    processor: ImageProcessor = app.processor
    await processor.delete_image(image_id)


@app.get("/images")
async def get_list_of_images() -> ImagesList:
    processor: ImageProcessor = app.processor
    data = await processor.get_list_of_image()
    return ImagesList(names=data)
