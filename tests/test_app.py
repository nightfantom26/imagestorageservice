import asyncio
from io import BytesIO
from unittest import TestCase
from unittest.mock import Mock

import pandas as pd
from starlette import status
from starlette.testclient import TestClient

from app import app
from schema import codes
from schema.status import Status


class TestAPI(TestCase):

    def test_success_upload_image(self):
        data = pd.DataFrame([[0] * 201])

        future = asyncio.Future()
        future.set_result(Status(code=codes.OK))

        buffer = BytesIO()
        data.to_csv(buffer)
        with TestClient(app) as client:
            app.processor = Mock()
            app.processor.upload_image = Mock(return_value=future)
            response = client.post("/image", files={"file": ("filename", buffer.getvalue(), "text/csv")})
            self.assertEqual(status.HTTP_201_CREATED, response.status_code)

    def test_upload_image_wrong_file_size(self):
        data = pd.DataFrame([[0] * 201])
        buffer = BytesIO()
        data.to_csv(buffer)
        with TestClient(app) as client:
            app.settings.file_size = 1
            response = client.post("/image", files={"file": ("filename", buffer.getvalue(), "text/csv")})
            self.assertEqual(status.HTTP_400_BAD_REQUEST, response.status_code)

    def test_upload_image_wrong_file_type(self):
        data = pd.DataFrame([[0] * 201])
        buffer = BytesIO()
        data.to_csv(buffer)
        with TestClient(app) as client:
            response = client.post("/image", files={"file": ("filename", buffer.getvalue(), "plain/csv")})
            self.assertEqual(status.HTTP_400_BAD_REQUEST, response.status_code)

    def test_get_image_frame_success(self):
        future = asyncio.Future()
        io = BytesIO()
        source = b"123"
        io.write(source)
        future.set_result((Status(code=codes.OK), io, "frame.jpg"))
        with TestClient(app) as client:
            app.processor = Mock()
            app.processor.get_image_frame = Mock(return_value=future)

            response = client.get("/image/1/", params={
                "depth_min": 1,
                "depth_max": 2,

            })
            bytes_response = list(response.iter_bytes())[0]
            self.assertEqual(source, bytes_response)
            self.assertEqual(status.HTTP_200_OK, response.status_code)

    def test_get_image_frame_fail(self):
        future = asyncio.Future()
        future.set_result((Status(code=codes.LOGICAL_ERROR), None, None))
        with TestClient(app) as client:
            app.processor = Mock()
            app.processor.get_image_frame = Mock(return_value=future)

            response = client.get("/image/1/", params={
                "depth_min": 1,
                "depth_max": 2,

            })
            self.assertEqual(status.HTTP_400_BAD_REQUEST, response.status_code)

    def test_delete_image_success(self):
        future = asyncio.Future()
        future.set_result(None)

        with TestClient(app) as client:
            app.processor = Mock()
            app.processor.delete_image = Mock(return_value=future)
            response = client.delete("/image/1")
            self.assertEqual(status.HTTP_200_OK, response.status_code)

    def test_get_list_of_images_success(self):
        source = ["1", "2"]
        future = asyncio.Future()
        future.set_result(source)

        with TestClient(app) as client:
            app.processor = Mock()
            app.processor.get_list_of_image = Mock(return_value=future)
            response = client.get("/images")
            self.assertEqual(status.HTTP_200_OK, response.status_code)
            self.assertDictEqual({"names": source}, response.json())
